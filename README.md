# pi-transcode

Scanner for transcode old video files(.avi) to .mp4.
This format is supported for plex and chromecast

For run this:

```
python transcode.py /dir/to/transcode
```

or with docker:

```
docker run -it --rm \
    -v /local/dir/to/transcode:/mnt  \
    -e TO_TRANSCODE=/mnt \
    nicost/pi-transcoder
```