#! /usr/bin/python3

import logging
import os
import subprocess
import sys
from pathlib import Path

logger = logging.getLogger()
logger.setLevel(logging.INFO)

handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def transcode(input_path: Path):
    if input_path.suffix in [".avi"]:
        output_path = input_path.with_suffix(".mp4")
        command = ["ffmpeg", "-loglevel", "error", "-stats", "-y", "-i", input_path, "-s", "hd720", output_path]
        try:
            logger.info(f"Processing {input_path}")
            subprocess.run(command, check=True, text=True)
        except Exception as e:
            logger.error(f"Something was wrong {e}")
        else:
            logger.info(f"Deleting {input_path}")
            input_path.unlink()
    else:
        logger.info(f"Nothing to do with {input_path}")


def surf_dir(path_dir: Path):
    if path_dir.name.startswith("."):
        return
    logger.info(f"Scanning {path_dir}")
    for item in path_dir.iterdir():
        if item.is_dir():
            surf_dir(item)
        else:
            transcode(item)


if __name__ == "__main__":
    try:
        env = os.environ.get("TO_TRANSCODE")
        path = Path(env) if env else Path(sys.argv[1])
    except IndexError:
        logger.warning("No path was defined")
        sys.exit()

    if path.exists():
        if path.is_dir():
            surf_dir(path)
        else:
            transcode(path)
